/*
 *  Synthesizer.cpp
 *
 *  Copyright (c) 2014-2019 Nick Dowell
 *
 *  This file is part of amsynth.
 *
 *  amsynth is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  amsynth is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with amsynth.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __amsynth__Synthesizer__
#define __amsynth__Synthesizer__

#include "types.h"
#include "controls.h"

#include <vector>
#include <string>
#include <map>
#include <iostream>

//
// Property names
//
#define PROP_KBM_FILE "tuning_kbm_file"
#define PROP_SCL_FILE "tuning_scl_file"


class MidiController;
class PresetController;
class VoiceAllocationUnit;

struct ISynthesizer
{
    virtual int loadTuningKeymap(const char *filename) = 0;
    virtual int loadTuningScale(const char *filename) = 0;
    
    virtual ~ISynthesizer() {}
};

class Synthesizer : ISynthesizer
{
public:
    
    Synthesizer();
    virtual ~Synthesizer();

    void valueChangedThroughMidi(std::string, float value);   // from the Helm API
    void prevPatch();
    void nextPatch();
    void noteOn(int key, float val);
    void noteOff(int key);

    std::map<std::string, int> gamepad_axis_mapping_;
    std::map<std::string, int> gamepad_btn_mapping_;

    // gamepad callbacks 
    void linkGamepadButton(const std::string& name, int index);
    void unlinkGamepadButton(const std::string& name);
    int getGamepadButtonLinkedTo(const std::string& name);

    void linkGamepadAxis(const std::string& name, int index);
    void unlinkGamepadAxis(const std::string& name);
    int getGamepadAxisLinkedTo(const std::string& name);

    void updateGamepad(
        float x1, float y1, float x2, float y2,  // first gamepad
        float x3, float y3, float x4, float y4,  // second gamepad
        float x5, float y5, float x6, float y6,  // third gamepad
        int b0,
        int b1,
        int b2,
        int b3,
        int b4,
        int b5,
        int b6,
        int b7,
        int b8,
        int b9,
        int b10,
        int b11,
        bool button_lock
    );


    void loadBank(const char *filename);
    void saveBank(const char *filename);

    void loadState(char *buffer);
    int saveState(char **buffer);

    int getPresetNumber();
    void setPresetNumber(int number);

    float getParameterValue(Param parameter);
    void setParameterValue(Param parameter, float value);

    float getNormalizedParameterValue(Param parameter);
    void setNormalizedParameterValue(Param parameter, float value);

    void getParameterName(Param parameter, char *buffer, size_t maxLen);
    void getParameterLabel(Param parameter, char *buffer, size_t maxLen);
    void getParameterDisplay(Param parameter, char *buffer, size_t maxLen);

    void setPitchBendRangeSemitones(int value);

	int getMaxNumVoices();
	void setMaxNumVoices(int value);

	virtual int loadTuningKeymap(const char *filename);
	virtual int loadTuningScale(const char *filename);

	void setSampleRate(int sampleRate);

	void process(unsigned nframes,
				 const std::vector<amsynth_midi_event_t> &midi_in,
				 std::vector<amsynth_midi_cc_t> &midi_out,
				 float *audio_l, float *audio_r, unsigned audio_stride = 1);

    MidiController *getMidiController() { return _midiController; };
    PresetController *getPresetController() { return _presetController; }

// private:

    double _sampleRate;
    MidiController *_midiController;
    PresetController *_presetController;
    VoiceAllocationUnit *_voiceAllocationUnit;
};

#endif /* defined(__amsynth__Synthesizer__) */
