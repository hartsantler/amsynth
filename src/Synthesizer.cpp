/*
 *  Synthesizer.cpp
 *
 *  Copyright (c) 2014-2019 Nick Dowell
 *
 *  This file is part of amsynth.
 *
 *  amsynth is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  amsynth is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with amsynth.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Synthesizer.h"

#include "Configuration.h"
#include "MidiController.h"
#include "PresetController.h"
#include "VoiceAllocationUnit.h"
#include "VoiceBoard/VoiceBoard.h"

#include <algorithm>
#include <cassert>
#include <cstdio>
#include <cstring>


Synthesizer::Synthesizer()
: _sampleRate(-1)
, _midiController(0)
, _presetController(0)
, _voiceAllocationUnit(0)
{
	Configuration &config = Configuration::get();

	_voiceAllocationUnit = new VoiceAllocationUnit;
	_voiceAllocationUnit->SetSampleRate((int) _sampleRate);
	_voiceAllocationUnit->SetMaxVoices(config.polyphony);
	_voiceAllocationUnit->setPitchBendRangeSemitones(config.pitch_bend_range);
	
	if (config.current_tuning_file != "default")
		_voiceAllocationUnit->loadScale(config.current_tuning_file.c_str());

	Preset::setIgnoredParameterNames(config.ignored_parameters);
	_presetController = new PresetController;
	_presetController->loadPresets(config.current_bank_file.c_str());
	_presetController->selectPreset(0);
	_presetController->getCurrentPreset().AddListenerToAll(_voiceAllocationUnit);
	
	_midiController = new MidiController();
	_midiController->SetMidiEventHandler(_voiceAllocationUnit);
	_midiController->setPresetController(*_presetController);
}

Synthesizer::~Synthesizer()
{
	delete _midiController;
	delete _presetController;
	delete _voiceAllocationUnit;
}

void Synthesizer::loadBank(const char *filename)
{
	_presetController->loadPresets(filename);
	_presetController->selectPreset(_presetController->getCurrPresetNumber());
}

void Synthesizer::saveBank(const char *filename)
{
	_presetController->commitPreset();
	_presetController->savePresets(filename);
}

void Synthesizer::loadState(char *buffer)
{
	if (!_presetController->getCurrentPreset().fromString(buffer))
		return;

	std::istringstream input (buffer);
	for (std::string line; std::getline(input, line); ) {
		std::istringstream stream (line);
		std::string type, key, value;
		stream >> type;

		if (type == "<property>") {
			stream >> key;
			stream.get(); // skip whitespace
			std::getline(stream, value); // value may contain whitespace

			if (key == PROP_KBM_FILE)
				loadTuningKeymap(value.c_str());

			if (key == PROP_SCL_FILE)
				loadTuningScale(value.c_str());
		}
	}
}

int Synthesizer::saveState(char **buffer)
{
	std::stringstream stream;
	_presetController->getCurrentPreset().toString(stream);

	const std::string &tuning_kbm_file = _voiceAllocationUnit->tuningMap.getKeyMapFile();
	if (tuning_kbm_file.length())
		stream << "<property> " PROP_KBM_FILE " " << tuning_kbm_file << std::endl;

	const std::string &tuning_scl_file = _voiceAllocationUnit->tuningMap.getScaleFile();
	if (tuning_scl_file.length())
		stream << "<property> " PROP_SCL_FILE " " << tuning_scl_file << std::endl;

	std::string string = stream.str();
	*buffer = (char *)malloc(4096);
	return sprintf(*buffer, "%s", string.c_str());
}

int Synthesizer::getPresetNumber()
{
	return _presetController->getCurrPresetNumber();
}

void Synthesizer::setPresetNumber(int number)
{
	_presetController->selectPreset(number);
}

float Synthesizer::getParameterValue(Param parameter)
{
	return _presetController->getCurrentPreset().getParameter(parameter).getValue();
}

float Synthesizer::getNormalizedParameterValue(Param parameter)
{
	return _presetController->getCurrentPreset().getParameter(parameter).GetNormalisedValue();
}

void Synthesizer::setParameterValue(Param parameter, float value)
{
	_presetController->getCurrentPreset().getParameter(parameter).setValue(value);
}

void Synthesizer::setNormalizedParameterValue(Param parameter, float value)
{
	_presetController->getCurrentPreset().getParameter(parameter).SetNormalisedValue(value);
}

void Synthesizer::getParameterName(Param parameter, char *buffer, size_t maxLen)
{
	strncpy(buffer, _presetController->getCurrentPreset().getParameter(parameter).getName().c_str(), maxLen);
}

void Synthesizer::getParameterLabel(Param parameter, char *buffer, size_t maxLen)
{
	strncpy(buffer, _presetController->getCurrentPreset().getParameter(parameter).getLabel().c_str(), maxLen);
}

void Synthesizer::getParameterDisplay(Param parameter, char *buffer, size_t maxLen)
{
	strncpy(buffer, _presetController->getCurrentPreset().getParameter(parameter).GetStringValue().c_str(), maxLen);
}

void Synthesizer::setPitchBendRangeSemitones(int value)
{
	_voiceAllocationUnit->setPitchBendRangeSemitones(value);
}

int Synthesizer::getMaxNumVoices()
{
	return _voiceAllocationUnit->GetMaxVoices();
}

void Synthesizer::setMaxNumVoices(int value)
{
	_voiceAllocationUnit->SetMaxVoices(value);
}

int Synthesizer::loadTuningKeymap(const char *filename)
{
	if (filename && strlen(filename))
		return _voiceAllocationUnit->loadKeyMap(filename);

	_voiceAllocationUnit->tuningMap.defaultKeyMap();
	return 0;
}

int Synthesizer::loadTuningScale(const char *filename)
{
	if (filename && strlen(filename))
		return _voiceAllocationUnit->loadScale(filename);

	_voiceAllocationUnit->tuningMap.defaultScale();
	return 0;
}

void Synthesizer::setSampleRate(int sampleRate)
{
	_sampleRate = sampleRate;
	_voiceAllocationUnit->SetSampleRate(sampleRate);
}

void Synthesizer::process(unsigned int nframes,
						  const std::vector<amsynth_midi_event_t> &midi_in,
						  std::vector<amsynth_midi_cc_t> &midi_out,
						  float *audio_l, float *audio_r, unsigned audio_stride)
{
	if (_sampleRate < 0) {
		assert(0 == "sample rate has not been set");
		return;
	}
	std::vector<amsynth_midi_event_t>::const_iterator event = midi_in.begin();
	unsigned frames_left_in_buffer = nframes, frame_index = 0;
	while (frames_left_in_buffer) {
		while (event != midi_in.end() && event->offset_frames <= frame_index) {
			_midiController->HandleMidiData(event->buffer, event->length);
			++event;
		}
		
		unsigned block_size_frames = std::min(frames_left_in_buffer, (unsigned)VoiceBoard::kMaxProcessBufferSize);
		if (event != midi_in.end() && event->offset_frames > frame_index) {
			unsigned frames_until_next_event = event->offset_frames - frame_index;
			block_size_frames = std::min(block_size_frames, frames_until_next_event);
		}
		
		_voiceAllocationUnit->Process(audio_l + (frame_index * audio_stride),
									  audio_r + (frame_index * audio_stride),
									  block_size_frames, audio_stride);
		
		frame_index += block_size_frames;
		frames_left_in_buffer -= block_size_frames;
	}
	while (event != midi_in.end()) {
		_midiController->HandleMidiData(event->buffer, event->length);
		++event;
	}
	_midiController->generateMidiOutput(midi_out);
}


// gamepad buttons
int Synthesizer::getGamepadButtonLinkedTo(const std::string& name) {
  if (this->gamepad_btn_mapping_.count(name) > 0)
    return this->gamepad_btn_mapping_[name];
  else
    return -1;
}
void Synthesizer::linkGamepadButton(const std::string& name, int index) {
  this->gamepad_btn_mapping_[name] = index;
}
void Synthesizer::unlinkGamepadButton(const std::string& name) {
  this->gamepad_btn_mapping_[name] = -1;
}

// analog sticks
int Synthesizer::getGamepadAxisLinkedTo(const std::string& name) {
  if (this->gamepad_axis_mapping_.count(name) > 0)
    return this->gamepad_axis_mapping_[name];
  else
    return -1;
}
void Synthesizer::linkGamepadAxis(const std::string& name, int index) {
  this->gamepad_axis_mapping_[name] = index;
}
void Synthesizer::unlinkGamepadAxis(const std::string& name) {
  this->gamepad_axis_mapping_[name] = -1;
}
void Synthesizer::updateGamepad(
  float x1, float y1, float x2, float y2, 
  float x3, float y3, float x4, float y4, 
  float x5, float y5, float x6, float y6, 
  int b0,
  int b1,
  int b2,
  int b3,
  int b4,
  int b5,
  int b6,
  int b7,
  int b8,
  int b9,
  int b10,
  int b11,
  bool button_lock
  ){

  for (auto& kw : this->gamepad_btn_mapping_) {
    int value = 0;
    if (kw.second == -1) continue;
    switch (kw.second) {
      case 0:
        value = b0;
        break;
      case 1:
        value = b1;
        break;
      case 2:
        value = b2;
        break;
      case 3:
        value = b3;
        break;
      case 4:
        value = b4;
        break;
      case 5:
        value = b5;
        break;
      case 6:
        value = b6;
        break;
      case 7:
        value = b7;
        break;
      case 8:
        value = b8;
        break;
      case 9:
        value = b9;
        break;
      case 10:
        value = b10;
        break;
      case 11:
        value = b11;
        break;
    }
    if (value == 1)
      this->valueChangedThroughMidi( kw.first, 1.0f );
    else if (value == -1 && button_lock==false)
      this->valueChangedThroughMidi( kw.first, 0.0f );
  }


  for (auto& kw : this->gamepad_axis_mapping_) {
    float value = 0.0;
    if (kw.second == -1) continue;
    switch (kw.second) {
      case 0:
        value = x1;
        break;
      case 1:
        value = y1;
        break;
      case 2:
        value = x2;
        break;
      case 3:
        value = y2;
        break;
      // optional second gamepad
      case 4:
        value = x3;
        break;
      case 5:
        value = y3;
        break;
      case 6:
        value = x4;
        break;
      case 7:
        value = y4;
        break;
      // optional third gamepad
      case 8:
        value = x5;
        break;
      case 9:
        value = y5;
        break;
      case 10:
        value = x6;
        break;
      case 11:
        value = y6;
        break;

    }
	value *= 0.5;
	value += 0.5;
    this->valueChangedThroughMidi( kw.first, value );
  }
}

static std::map<std::string, Param> str2param = {
	// minimal helm compat - not precise
	{"osc_feedback_amount",kAmsynthParameter_Oscillator2Detune},
	{"cutoff", kAmsynthParameter_FilterCutoff},
	{"portamento",kAmsynthParameter_PortamentoMode},
	{"resonance",kAmsynthParameter_FilterResonance},

  {"arp_gate",kAmsynthParameter_LFOToAmp},
  {"sub_shuffle",kAmsynthParameter_LFOToOscillators},
  {"beats_per_minute",kAmsynthParameter_PortamentoTime},
  {"formant_x",kAmsynthParameter_Oscillator2Octave},
  {"formant_y",kAmsynthParameter_OscillatorMix},
  {"osc_1_tune",kAmsynthParameter_Oscillator2Pitch},
  {"delay_feedback",kAmsynthParameter_AmpDistortion},
  {"osc_2_tune",kAmsynthParameter_FilterResonance},
  {"cross_modulation",kAmsynthParameter_Oscillator2Waveform},

	// amsynth params
	{"AmpEnvAttack", kAmsynthParameter_AmpEnvAttack},
	{"AmpEnvDecay",kAmsynthParameter_AmpEnvDecay},
	{"AmpEnvSustain",kAmsynthParameter_AmpEnvSustain},
	{"AmpEnvRelease",kAmsynthParameter_AmpEnvRelease},
	
	{"Oscillator1Waveform",kAmsynthParameter_Oscillator1Waveform},
	
	{"FilterEnvAttack",kAmsynthParameter_FilterEnvAttack},
	{"FilterEnvDecay",kAmsynthParameter_FilterEnvDecay},
	{"FilterEnvSustain",kAmsynthParameter_FilterEnvSustain},
	{"FilterEnvRelease",kAmsynthParameter_FilterEnvRelease},
	{"FilterResonance",kAmsynthParameter_FilterResonance},
	{"FilterEnvAmount",kAmsynthParameter_FilterEnvAmount},
	{"FilterCutoff",kAmsynthParameter_FilterCutoff},
	
	{"Oscillator2Detune",kAmsynthParameter_Oscillator2Detune},
	{"Oscillator2Waveform",kAmsynthParameter_Oscillator2Waveform},
	
	{"MasterVolume",kAmsynthParameter_MasterVolume},
	
	{"LFOFreq",kAmsynthParameter_LFOFreq},
	{"LFOWaveform",kAmsynthParameter_LFOWaveform},
	
	{"Oscillator2Octave",kAmsynthParameter_Oscillator2Octave},
	{"OscillatorMix",kAmsynthParameter_OscillatorMix},
	
	{"LFOToOscillators",kAmsynthParameter_LFOToOscillators},
	{"LFOToFilterCutoff",kAmsynthParameter_LFOToFilterCutoff},
	{"LFOToAmp",kAmsynthParameter_LFOToAmp},
	
	{"OscillatorMixRingMod",kAmsynthParameter_OscillatorMixRingMod},
	
	{"Oscillator1Pulsewidth",kAmsynthParameter_Oscillator1Pulsewidth},
	{"Oscillator2Pulsewidth",kAmsynthParameter_Oscillator2Pulsewidth},
	
	{"ReverbRoomsize",kAmsynthParameter_ReverbRoomsize},
	{"ReverbDamp",kAmsynthParameter_ReverbDamp},
	{"ReverbWet",kAmsynthParameter_ReverbWet},
	{"ReverbWidth",kAmsynthParameter_ReverbWidth},
	
	{"AmpDistortion",kAmsynthParameter_AmpDistortion},
	
	{"Oscillator2Sync",kAmsynthParameter_Oscillator2Sync},

	{"PortamentoTime",kAmsynthParameter_PortamentoTime},
	
	{"KeyboardMode",kAmsynthParameter_KeyboardMode},

	{"Oscillator2Pitch",kAmsynthParameter_Oscillator2Pitch},
	{"FilterType",kAmsynthParameter_FilterType},
	{"FilterSlope",kAmsynthParameter_FilterSlope},

	{"LFOOscillatorSelect",kAmsynthParameter_LFOOscillatorSelect},

	{"FilterKeyTrackAmount",kAmsynthParameter_FilterKeyTrackAmount},
	{"FilterKeyVelocityAmount",kAmsynthParameter_FilterKeyVelocityAmount},
	
	{"AmpVelocityAmount",kAmsynthParameter_AmpVelocityAmount},
	
	{"PortamentoMode",kAmsynthParameter_PortamentoMode},

};

void Synthesizer::valueChangedThroughMidi(std::string key, float value) {
	if (str2param.count(key)==0) {
		std::cout << "ERROR: invalid param key - " << key << std::endl;
		return;
	}
	//this->setParameterValue(str2param[key], value);
	this->setNormalizedParameterValue(str2param[key], value);
}

void Synthesizer::prevPatch() {
  int p = this->getPresetNumber();
  std::cout << "current patch: " << p << std::endl;
  this->setPresetNumber(p-1);
}

void Synthesizer::nextPatch() {
  int p = this->getPresetNumber();
  std::cout << "current patch: " << p << std::endl;
  this->setPresetNumber(p+1);
}

void Synthesizer::noteOn(int key, float vel) {
  if (vel < 0.0)
    vel = 0.0;
  if (vel > 1.0)
    vel = 1.0;
	if (key>0 and key < 128)
		this->_midiController->noteOn(key, vel);
}

void Synthesizer::noteOff(int key) {
	if (key>0 and key < 128)
		this->_midiController->noteOff(key, 0.0);
}


